package at.kolb.cars;

public class CarStarter {

	public static void main(String[] args) {
		
		Engine e1 = new Engine("Diesel",110);
		Manufacturer m1 = new Manufacturer("VW","Deutschland",10);
		Car c1 = new Car("red",180,20000,10,e1,m1,50001);
		Car c2 = new Car("blue",200,45000,11,e1,m1,500);
		Person p1 = new Person("Michael","M�ller",1151985);
		
		System.out.println("Mit Rabatt koste ich "+c1.getPrice()+"�");
		System.out.println("Ich verbrauche "+c1.getUsage()+" liter");
		
		p1.addcar(c1);
		p1.addcar(c2);
		p1.valueCars();
		
		
	}

}