package at.kolb.cars;

import java.util.ArrayList;
import java.util.List;

public class Person {
	
	private String name;
	private String lastName;
	private int birthday;
	private Car Car;
	public List<Car> Cars;
	
	

	public Person(String name, String lastName, int birthday) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.birthday = birthday;
		this.Cars = new ArrayList<>();
		
	}
	public void addcar(Car Cars) {
		
		this.Cars.add(Cars);
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getBirthday() {
		return birthday;
	}
	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}
	public List<Car> getCars() {
		return Cars;
	}
	public void setCars(List<Car> cars) {
		Cars = cars;
	}
	public void valueCars() {
		 int Price = 0;
		 for (Car counter : Cars) {
			 Price = Price + counter.getPrice();
			 
		 }

		 System.out.println(Price);
	
	}
	
}
