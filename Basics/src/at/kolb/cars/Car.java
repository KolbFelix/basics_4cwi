package at.kolb.cars;


public class Car {

	private String color;
	private int vMax;
	private int price;
	private double usage;
	private Engine motor;
	private Manufacturer manu;
	private int Kilometerstand;


	public Car(String color, int vMax, int price, double usage, Engine motor, Manufacturer manu, int kilometerstand) {
		this.color = color;
		this.vMax = vMax;
		this.price = price;
		this.usage = usage;
		this.motor = motor;
		this.manu = manu;
		Kilometerstand = kilometerstand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getVmax() {
		return vMax;
	}

	public void setVmax(int vmax) {
		vMax = vmax;
	}

	public int getPrice() {
		return (this.price - this.price / 100 * manu.getDiscount());
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getUsage() {
		 if(this.Kilometerstand >= 50000) {
				
			return	this.usage + this.usage / 100 * 9.8;
	
		}
		 else {
			 return this.usage;
		 }
	}
	public void setUsage(double usage) {
		this.usage = usage;
	}

	public Engine getMotor() {
		return motor;
	}

	public void setMotor(Engine motor) {
		this.motor = motor;
	}

	public Manufacturer getManu() {
		return manu;
	}

	public void setManu(Manufacturer manu) {
		this.manu = manu;
	}

	public int getKilometerstand() {
		return Kilometerstand;
	}

	public void setKilometerstand(int kilometerstand) {
		Kilometerstand = kilometerstand;
	}
	
	
	
}
