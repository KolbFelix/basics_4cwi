package at.kolb.cars;

public class Engine {

	private String engine;
	private int horsePower;
	public Engine(String engine, int horsePower) {
		this.engine = engine;
		this.horsePower = horsePower;
	}
	
	public String getEngine() {
		return engine;
	}
	public void setEngine(String engine) {
		this.engine = engine;
	}
	public int getHorsePower() {
		return horsePower;
	}
	public void setHorsePower(int horsePower) {
		this.horsePower = horsePower;
	}
	
	
}
