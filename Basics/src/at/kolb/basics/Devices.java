package at.kolb.basics;

public class Devices {

	private int serialNumber;
	private type deviceType;
	
	public enum type {
		TV, Beamer, Radio
		
	}

	public Devices(int serialNumber, type deviceType) {
		super();
		this.serialNumber = serialNumber;
		this.deviceType = deviceType;
	}
	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public type getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(type deviceType) {
		this.deviceType = deviceType;
	}
	
	
	
}
