package at.kolb.basics;

import java.util.ArrayList;
import java.util.List;

public class Remote {

	private int lenght;
	private int width;
	private int weight;
	private int Serialnumber;
	public List<Devices> device;


	public Remote(int lenght, int width, int weight, int serialnumber) {
		super();
		this.lenght = lenght;
		this.width = width;
		this.weight = weight;
		this.Serialnumber = serialnumber;
		this.device = new ArrayList<>();
	}
	
	public int getLenght() {
		return lenght;
	}




	public void setLenght(int lenght) {
		this.lenght = lenght;
	}




	public int getWidth() {
		return width;
	}




	public void setWidth(int width) {
		this.width = width;
	}




	public int getWeight() {
		return weight;
	}




	public void setWeight(int weight) {
		this.weight = weight;
	}




	public int getSerialnumber() {
		return Serialnumber;
	}




	public void setSerialnumber(int serialnumber) {
		Serialnumber = serialnumber;
	}




	public List<Devices> getDevice() {
		return device;
	}




	public void setDevice(List<Devices> device) {
		this.device = device;
	}




	public void adddevice(Devices device) {
		
		this.device.add(device);
		
	}

	public void turnON() {
		System.out.println("I am turned on now");

	}

	public void turnOFF() {
		System.out.println("I am turned OFF now");

	}

	public void SayHello() {
		System.out.println("I have the following values: " + this.Serialnumber);

	}

	public void getSerialNumber() {
		this.Serialnumber = 12345;
		
	}

}
