package at.kolb.basics;

public class RemoteStarter {
	public static void main(String[] args) {

		
		
		Devices d1 = new Devices(4465442, Devices.type.Beamer); 
		Devices d2 = new Devices(4545221, Devices.type.TV);
		Remote r1 = new Remote(70, 20, 300, 123);
		Remote r2 = new Remote(70, 20, 300, 123456789);
		Remote r3 = r2;
		Batterie b1 = new Batterie("123A",150,200,"Duracel");
		Batterie b2 = new Batterie("123B",150,200,"S-budget");
		
	
		
		r1.adddevice(d1);
		r1.adddevice(d2);
		
		System.out.println(r1.getDevice().get(1).getSerialNumber());
		
		r1.SayHello();
		r2.SayHello();

		r2.getSerialNumber();

		r2.SayHello();
		r3.SayHello();
		
		b1.SayHello();
	}

	
}
